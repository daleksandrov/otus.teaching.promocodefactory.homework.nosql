﻿namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions
{
    public interface IMongoDbSettings
    {
        string DatabaseName { get; set; }
        string ConnectionString { get; set; }
    }
}