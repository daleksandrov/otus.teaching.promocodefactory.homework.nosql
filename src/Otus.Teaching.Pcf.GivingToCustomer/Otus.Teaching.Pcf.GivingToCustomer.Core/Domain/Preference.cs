﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    [BsonCollection("preferences")]
    public class Preference
        :Document
    {
        public string Name { get; set; }
    }
}