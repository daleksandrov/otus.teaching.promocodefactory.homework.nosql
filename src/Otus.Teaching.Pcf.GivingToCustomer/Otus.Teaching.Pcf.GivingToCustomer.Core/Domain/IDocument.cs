﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public interface IDocument
    {
        [BsonId]
        Guid Id { get; set; }
    }
}