﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    [BsonCollection("customerPreferences")]
    public class CustomerPreference 
        :Document
    {
        public Guid CustomerId { get; set; }
        public Guid PreferenceId { get; set; }
    }
}