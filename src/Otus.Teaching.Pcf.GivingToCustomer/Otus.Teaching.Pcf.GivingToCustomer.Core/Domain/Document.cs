﻿using System;
using MongoDB.Bson;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public abstract class Document : IDocument
    {
        public Guid Id { get; set; }
    }
}