﻿using System.Collections.Generic;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    [BsonCollection("customers")]
    public class Customer
        :Document
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public virtual ICollection<Preference> Preferences { get; set; }
        
        public virtual ICollection<PromoCodeCustomer> PromoCodes { get; set; }
    }
}