﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    [BsonCollection("promoCodeCustomers")]
    public class PromoCodeCustomer : Document
    {
        public Guid PromoCodeId { get; set; }

        public Guid CustomerId { get; set; }
    }
}
