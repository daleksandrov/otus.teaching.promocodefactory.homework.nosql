﻿using System;
using Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Data;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests
{
    public class MongoDatabaseFixture: IDisposable
    {
        private readonly TestMongoDbInitializer _testMongoDbInitializer;
        
        public MongoDatabaseFixture()
        {
            mongoDbSettings = new MongoDbSettings()
            {
                ConnectionString = "mongodb://mongoadmin:_Test123@localhost",
                DatabaseName = "TestDataBase"
            };

            _testMongoDbInitializer= new TestMongoDbInitializer(mongoDbSettings);
            _testMongoDbInitializer.InitializeDb();
        }

        public void Dispose()
        {
            _testMongoDbInitializer.CleanDb();
        }
        
        public MongoDbSettings mongoDbSettings { get; private set; }
    }
}