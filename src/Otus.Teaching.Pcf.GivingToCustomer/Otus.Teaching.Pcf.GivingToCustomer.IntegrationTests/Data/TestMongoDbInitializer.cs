﻿using System;
using System.Linq;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Data
{
    public class TestMongoDbInitializer : IDbInitializer
    {
        private readonly IMongoCollection<Preference> _collectionPreferences;
        private readonly IMongoCollection<Customer> _collectionCustomers;
        private readonly IMongoDatabase _database;
        
        public TestMongoDbInitializer(IMongoDbSettings settings)
        {
            _database = new MongoClient(settings.ConnectionString).GetDatabase(settings.DatabaseName);
            _collectionPreferences = _database.GetCollection<Preference>(GetCollectionName(typeof(Preference)));
            _collectionCustomers = _database.GetCollection<Customer>(GetCollectionName(typeof(Customer)));
        }
        
        private protected string GetCollectionName(Type documentType)
        {
            return ((BsonCollectionAttribute) documentType.GetCustomAttributes(
                    typeof(BsonCollectionAttribute),
                    true)
                .FirstOrDefault())?.CollectionName;
        }
        public void InitializeDb()
        {
            if(!_collectionPreferences.AsQueryable().Any())
                _collectionPreferences.InsertMany(FakeDataFactory.Preferences);
            
            if(!_collectionCustomers.AsQueryable().Any())
                _collectionCustomers.InsertMany(FakeDataFactory.Customers);
        }

        public void CleanDb()
        {
            _database.DropCollectionAsync(GetCollectionName(typeof(Preference)));
            _database.DropCollectionAsync(GetCollectionName(typeof(Customer)));
        }
    }
}