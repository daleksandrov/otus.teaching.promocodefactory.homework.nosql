﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}