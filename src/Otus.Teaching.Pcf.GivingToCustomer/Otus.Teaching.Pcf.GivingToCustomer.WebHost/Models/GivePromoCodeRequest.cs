﻿using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models
{
    /// <example>
    /// {
    ///    "ServiceInfo": "Такси",
    ///    "PartnerId": "3CFB693D-AA7D-4AAE-A5D5-26646DFEA1B2",
    ///    "PromoCode": "CODE1",
    ///    "PreferenceId": "c4bda62e-fc74-4256-a956-4760b3858cbd",
    ///    "BeginDate": "2020-05-01", 
    ///    "EndDate": "2020-05-01"
    /// }
    /// </example>>
    public class GivePromoCodeRequest
    {
        public string ServiceInfo { get; set; }

        public Guid PartnerId { get; set; }

        public string PromoCode { get; set; }

        public Guid PreferenceId { get; set; }

        public string BeginDate { get; set; }

        public string EndDate { get; set; }
    }
}