﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost
{
    public class MongoDbSettings : IMongoDbSettings
    {
        public string DatabaseName { get; set; }
        public string ConnectionString { get; set; }
    }
}