﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IMongoRepository<PromoCode> _promoCodesRepository;
        private readonly IMongoRepository<Preference> _preferencesRepository;
        private readonly IMongoRepository<Customer> _customersRepository;

        public PromocodesController(IMongoRepository<PromoCode> promoCodesRepository, 
            IMongoRepository<Preference> preferencesRepository, IMongoRepository<Customer> customersRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
        }
        
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<List<PromoCodeShortResponse>> GetPromocodes()
        {
            var promocodes = _promoCodesRepository.AsQueryable();

            var response = promocodes.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                BeginDate = x.BeginDate.ToString("yyyy-MM-dd"),
                EndDate = x.EndDate.ToString("yyyy-MM-dd"),
                PartnerId = x.PartnerId,
                ServiceInfo = x.ServiceInfo
            }).ToList();

            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //Получаем предпочтение
            var preference = await _preferencesRepository.FindByIdAsync(request.PreferenceId);

            if (preference == null)
            {
                return BadRequest();
            }

            //  Получаем клиентов с этим предпочтением:
            var customers = _customersRepository
                .AsQueryable().Where(d => d.Preferences.Any(x =>
                    x.Id == preference.Id));

            PromoCode promoCode = PromoCodeMapper.MapFromModel(request, preference, customers);

            //Добавляем промокоды клиентам
            foreach (var customer in customers)
            {
                if (customer.PromoCodes == null)
                    customer.PromoCodes = new List<PromoCodeCustomer>();
                
                customer.PromoCodes.Add(new PromoCodeCustomer()
                {
                    CustomerId = customer.Id,
                    PromoCodeId = promoCode.Id
                });
                
                await _customersRepository.ReplaceOneAsync(customer);
            }

            await _promoCodesRepository.InsertOneAsync(promoCode);

            return CreatedAtAction(nameof(GetPromocodes), new { }, null);
        }
    }
}