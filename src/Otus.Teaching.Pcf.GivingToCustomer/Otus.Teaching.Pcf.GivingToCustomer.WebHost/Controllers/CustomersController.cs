﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IMongoRepository<Customer> _customerRepository;
        private readonly IMongoRepository<Preference> _preferenceRepository;
        private readonly IMongoRepository<PromoCode> _promoCodeRepository;

        public CustomersController(IMongoRepository<Customer> customerRepository, 
            IMongoRepository<Preference> preferenceRepository, 
            IMongoRepository<PromoCode> promoCodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promoCodeRepository = promoCodeRepository;
        }
        
        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<List<CustomerShortResponse>> GetCustomers()
        {
            var customers =  _customerRepository.FilterBy(filter => 
                filter.Id != null,
                projecton => new CustomerShortResponse()
                {
                    Id = Guid.Parse(projecton.Id.ToString()),
                    Email = projecton.Email,
                    FirstName = projecton.FirstName,
                    LastName = projecton.LastName
                }).ToList();

            return Ok(customers);
        }

        /// <summary>
        /// Получить клиента по id
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.FindByIdAsync(id);

            if (customer == null)
                return NotFound();

            IEnumerable<PromoCode> promocodes = null;

            if (customer.PromoCodes != null)
            {
                promocodes =
                    await _promoCodeRepository.FindRangeByIdsAsync(customer.PromoCodes.Select(x => x.PromoCodeId)
                        .ToList());
            }

            var response = new CustomerResponse(customer, promocodes);

            return Ok(response);
        }
        
        /// <summary>
        /// Создать нового клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences =
                await _preferenceRepository.FindRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);
            
            await _customerRepository.InsertOneAsync(customer);
        
            return CreatedAtAction(nameof(GetCustomerAsync), new {id = customer.Id}, customer.Id);
        }
        
        /// <summary>
        /// Обновить клиента
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        /// <param name="request">Данные запроса></param>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.FindByIdAsync(id);
            
            if (customer == null)
                return NotFound();
            
            var preferences =
                await _preferenceRepository.FindRangeByIdsAsync(request.PreferenceIds);
            
            //TODO: сделать так чтобы при обновлении клиента не затирались промокоды
            
            CustomerMapper.MapFromModel(request, preferences, customer);
        
            await _customerRepository.ReplaceOneAsync(customer);
        
            return NoContent();
        }
        
        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            await _customerRepository.DeleteByIdAsync(id);
        
            return NoContent();
        }
    }
}